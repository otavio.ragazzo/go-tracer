module gitlab.com/otavio3/go-tracer

replace gopkg.in/DataDog/dd-trace-go.v1 => ./gitlab.com/otavio3/go-tracer

go 1.15

require (
	cloud.google.com/go/pubsub v1.10.1
	github.com/DataDog/datadog-go v4.4.0+incompatible
	github.com/Microsoft/go-winio v0.4.16 // indirect
	github.com/Shopify/sarama v1.28.0
	github.com/aws/aws-sdk-go v1.37.28
	github.com/bradfitz/gomemcache v0.0.0-20190913173617-a41fca850d0b
	github.com/confluentinc/confluent-kafka-go v1.6.1
	github.com/emicklei/go-restful v2.15.0+incompatible
	github.com/garyburd/redigo v1.6.2
	github.com/gin-gonic/gin v1.6.3
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/go-chi/chi v1.5.4
	github.com/go-pg/pg/v10 v10.8.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-redis/redis/v8 v8.7.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gocql/gocql v0.0.0-20210310132943-486542b7b4b4
	github.com/gofiber/fiber/v2 v2.5.0
	github.com/golang/protobuf v1.4.3
	github.com/gomodule/redigo v1.8.4
	github.com/google/pprof v0.0.0-20210125172800-10e9aeb4a998
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/graph-gophers/graphql-go v0.0.0-20210306090651-bd703c223f03
	github.com/hashicorp/consul/api v1.8.1
	github.com/hashicorp/vault/api v1.0.4
	github.com/hashicorp/vault/sdk v0.1.13
	github.com/jackc/pgx/v4 v4.10.1
	github.com/jinzhu/gorm v1.9.16
	github.com/jmoiron/sqlx v1.3.1
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/julienschmidt/httprouter v1.3.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.2.1
	github.com/lib/pq v1.10.0
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/miekg/dns v1.1.40
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/opentracing/opentracing-go v1.2.0
	github.com/philhofer/fwd v1.1.1 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/syndtr/goleveldb v1.0.0
	github.com/tidwall/buntdb v1.2.0
	github.com/tinylib/msgp v1.1.2
	github.com/twitchtv/twirp v7.1.1+incompatible
	github.com/zenazn/goji v1.0.1
	go.mongodb.org/mongo-driver v1.5.0
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110
	golang.org/x/oauth2 v0.0.0-20210311163135-5366d9dc1934
	golang.org/x/sys v0.0.0-20210305230114-8fe3ee5dd75b
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
	google.golang.org/api v0.41.0
	google.golang.org/grpc v1.36.0
	gopkg.in/jinzhu/gorm.v1 v1.9.2
	gopkg.in/olivere/elastic.v3 v3.0.75
	gopkg.in/olivere/elastic.v5 v5.0.86
	gorm.io/driver/mysql v1.0.5
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.3
)
